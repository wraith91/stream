var elixir = require('laravel-elixir');
require('laravel-elixir-imagemin');
require('laravel-elixir-sass-compass');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.images = {
    folder: 'images',
    outputFolder: 'main/images'
};

elixir(function(mix) {
	mix.compass('app.scss', 'public/main/css/', {
      sass: "resources/assets/sass",
      font: "public/main/fonts",
      image: "public/main/images",
      javascript: "public/main/js",
      sourcemap: false
    })
    .imagemin()
    .copy('resources/assets/fonts', 'public/main/fonts')
    .copy('resources/assets/components/slick-carousel/slick/fonts', 'public/main/fonts')
    .copy('resources/assets/js', 'public/main/js')
    .copy('resources/assets/components/jquery/dist/jquery.min.js', 'public/main/js')
    .copy('resources/assets/components/jquery-infinite-scroll/jquery.infinitescroll.min.js', 'public/main/js')
    .copy('resources/assets/components/masonry/dist/masonry.pkgd.min.js', 'public/main/js')
    .copy('resources/assets/components/slick-carousel/slick/slick.js', 'public/main/js')
    .copy('resources/assets/components/swipebox/src/js/jquery.swipebox.js', 'public/main/js')
    .copy('resources/assets/components/select2/dist/js/select2.js', 'public/main/js')
    .copy('resources/assets/components/select2/dist/css/select2.min.css', 'public/main/css')

    .scripts([
        'autocomplete.js',
        'swipebox.js',
        'slick.script.js',
    ], 'public/main/js/dist/all.js', 'public/main/js');
});
