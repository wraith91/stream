<?php

use App\Acme\JobRole;
use Illuminate\Database\Seeder;

class JobRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_roles')->delete();

        $roles = ['Director', 'Cast'];

        foreach ($roles as $value) {
            JobRole::create([
                'name' => $value
            ]);
        }
    }
}
