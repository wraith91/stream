<?php

use App\Acme\Country;

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('countries')->delete();

        $countries = [
            'Amerika',
            'Australia',
            'China',
            'Perancis',
            'Jerman',
            'Hongkong',
            'India',
            'Inggris',
            'Iran',
            'Israel',
            'Itali',
            'Jepang',
            'Kanada',
            'Korea',
            'Malaysia',
            'Meksiko',
            'Pilipina',
            'Romania',
            'Rusia',
            'Saudi Arabia',
            'Taiwan',
            'Tailand'
        ];

        foreach ($countries as $value) {
            Country::create([
                'name' => $value
            ]);
        }
    }
} 