<?php

use App\Acme\People;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->delete();

        $people = [
            'Adam Levine', 'Adele', 'Alexander Skarsgard', 'Ali Larter', 'Alicia Keys', 'Amanda Bynes', 'Amanda Seyfried', 'America Ferrera',
            'Amy Adams', 'Amy Winehouse', 'Andrew Garfield', 'Angelina Jolie', 'Anna Nicole Smith', 'Anna Paquin', 'Anne Hathaway', 
            'Ashlee Simpson', 'Adrianne Palicki', 'Ashley Greene', 'Ashley Olsen', 'Ashley Tisdale', 'Ashton Kutcher', 'Audrina Patridge',
            'Avril Lavigne', 'Alicia Vikander', 'Alexandra Daddario', 'Ana de Armas', 'Amber Heard', 'Anthony Mackie', 'Anthony Russo',

            'Ben Affleck', 'Bethenny Frankel', 'Beyoncé Knowles', 'Blake Lively', 'Blake Shelton', 'Brad Paisley', 'Brad Pitt', 'Bradley Cooper',
            'Britney Spears', 'Brody Jenner', 'Brooke Shields', 'Bruce Willis', 'Benn',

            'Céline Dion', 'Cameron Diaz', 'Carmen Electra', 'Carrie Underwood', 'Cate Blanchett', 'Catherine Zeta-Jones', 'Chace Crawford',
            'Channing Tatum', 'Charlie Sheen', 'Charlize Theron', 'Cheryl Burke', 'Chris Evans', 'Chris Brown', 'Chris Hemsworth', 'Chris Pine', 
            'Christian Bale', 'Christina Aguilera', 'Christina Applegate', 'Christina Hendricks', 'Claire Danes', 'Clay Aiken', 'Colin Farrell', 
            'Colin Firth', 'Corbin Bleu', 'Cory Monteith', 'Courteney Cox', 'Chadwick Boseman', 'Camille Delamarre',

            'Dakota Fanning', 'Daniel Craig', 'Daniel Radcliffe', 'David Archuleta', 'David Beckham', 'David Cook', 'Demi Lovato', 'Demi Moore', 
            'Denise Richards', 'Denzel Washington', 'Diddy', 'Drew Barrymore', 'Daisy Ridley', 'Daniel Brühl', 'Don Cheadle',

            'Ed Westwick', 'Ed Skrein', 'Elin Nordegren', 'Elisabeth Hasselbeck', 'Elizabeth Olsen', 'Ellen DeGeneres', 'Ellen Pompeo', 
            'Emily Blunt', 'Emilia Clarke', 'Emma Roberts', 'Emma Stone', 'Emma Watson', 'Eva Longoria', 'Eva Mendes', 'Evan Rachel Wood', 
            'Evangeline Lilly', 'Ellie Kemper', 'Emily VanCamp',

            'Faith Hill', 'Fergie', 'Frank Grillo',

            'George Clooney', 'Gerard Butler', 'Gisele Bündchen', 'Gwen Stefani', 'Gwyneth Paltrow', 'Gal Gadot', 

            'Halle Berry', 'Hayden Panettiere', 'Heath Ledger', 'Heather Locklear', 'Heidi Klum', 'Heidi Montag', 'Hilary Duff', 'Hugh Jackman',
            'Henry Cavill',

            'Isla Fisher',

            'Jake Gyllenhaal', 'James Franco', 'Jamie Lynn Spears', 'Janet Jackson', 'January Jones', 'Jennifer Aniston', 'Jennifer Garner', 
            'Jennifer Hudson', 'Jennifer Lawrence', 'Jennifer Lopez', 'Jenny McCarthy', 'Jessica Alba', 'Jessica Biel', 'Jessica Chastain', 
            'Jessica Simpson', 'Jessica Szohr', 'Joe Jonas', 'Joel Madden', 'John Krasinski', 'John Mayer', 'Johnny Depp', 'Jon Hamm', 
            'Jonas Brothers', 'Jordin Sparks', 'Josh Duhamel', 'Josh Hartnett', 'Josh Hutcherson', 'Jude Law', 'Julia Louis-Dreyfus', 
            'Julia Roberts', 'Julianne Hough', 'Justin Bieber', 'Justin Timberlake', 'Jeremy Renner', 'Joe Russo',

            'Kanye West', 'Kate Beckinsale', 'Kate Bosworth', 'Kate Gosselin', 'Kate Hudson', 'Princess Kate', 'Kate Moss', 'Kate Walsh',
            'Kate Winslet', 'Katharine McPhee', 'Katherine Heigl', 'Katie Holmes', 'Katy Perry', 'Keanu Reeves', 'Keira Knightley', 'Keith Urban',
            'Kellan Lutz', 'Kellie Pickler', 'Kelly Clarkson', 'Kelly Ripa', 'Kendra Wilkinson', 'Kenny Chesney', 'Keri Russell', 'Kerry Washington',
            'Kevin Federline', 'Khloé Kardashian', 'Kim Kardashian', 'Kirsten Dunst', 'Kirstie Alley', 'Kourtney Kardashian','Kris Allen',
            'Kristen Bell', 'Kristen Stewart', 'Kristin Cavallari', 'Kit Harington', 

            'Lady Gaga', 'Lauren Conrad', 'Lea Michele', 'LeAnn Rimes', 'Leighton Meester', 'Leonardo DiCaprio', 'Liam Hemsworth', 'Lindsay Lohan',
            'Liv Tyler', 'Lucy Liu', 'Lena Headey',

            'Madonna', 'Maggie Gyllenhaal', 'Mandy Moore', 'Mariah Carey', 'Mario Lopez', 'Mark Ballas', 'Mark Wahlberg', 'Mary-Kate Olsen',
            'Matt Damon', 'Matthew McConaughey', 'Megan Fox', 'Michelle Obama', 'Michelle Williams', 'Mila Kunis', 'Miley Cyrus', 'Milo Ventimiglia',
            'Miranda Kerr', 'Miranda Lambert', 'Mischa Barton', 'Morena Baccarin', 'Margot Robbie', 'Marisa Tomei', 'Martin Freeman',

            'Naomi Campbell', 'Naomi Watts', 'Natalie Portman', 'Neil Patrick Harris', 'Nick Lachey', 'Nicole Kidman', 'Nicole Richie',
            'Natalie Dormer',

            'Olivia Wilde', 'Oprah Winfrey', 'Orlando Bloom', 'Owen Wilson', 'Olivia Munn',

            'Pamela Anderson', 'Paris Hilton', 'Patrick Dempsey', 'Paula Abdul', 'Penélope Cruz', 'Penn Badgley', 'Pete Wentz', 'Pink',
            'Pippa Middleton', 'Prince Harry', 'Prince William', 'Paul Bettany', 'Paul Rudd',

            'Queen Latifah',

            'Rachael Ray', 'Rachel Bilson', 'Rachel McAdams', 'Rebecca Romijn', 'Reese Witherspoon', 'Renée Zellweger', 'Rihanna', 
            'Robert Downey Jr.', 'Robert Pattinson', 'Rooney Mara', 'Rosario Dawson', 'Rosie O-Donnell', 'Rumer Willis', 'Ryan Gosling',
            'Ryan Phillippe', 'Ryan Reynolds', 'Ryan Seacrest',

            'Salma Hayek', 'Sandra Bullock', 'Sandra Oh', 'Sarah Jessica Parker', 'Sarah Michelle Gellar', 'Scarlett Johansson', 'Selena Gomez',
            'Shakira', 'Shania Twain', 'Sheryl Crow', 'Shia LaBeouf', 'Sienna Miller', 'Simon Cowell', 'Sofia Vergara', 'Sebastian Stan', 
            'Sophie Turner', 'Sam Heughan', 'Sam Claflin',

            'Taylor Hicks', 'Taylor Lautner', 'Taylor Momsen', 'Taylor Swift', 'Teri Hatcher', 'Tiger Woods', 'Tim McGraw', 'Tina Fey',
            'Tom Brady', 'Tom Cruise', 'Tori Spelling', 'Tyra Banks', 'Tom Holland', 'Tom Hardy', 'Tom Hiddleston', 'Tremaine Neverson', 
            'Tim Miller', 'TJ Miller',

            'Usher',

            'Vanessa Hudgens', 'Vanessa Minnillo', 'Vanessa Williams', 'Victoria Beckham', 'Vince Vaughn',

            'Whitney Port', 'Will Smith', 'Winona Ryder',

            'Zac Efron', 'Zoë Saldana',
        ];

        foreach ($people as $i => $value) {
            $lists[] = [
                'name' => $value,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }

        People::insert($lists);
    }
}
