<?php

use App\Acme\Genre;
use Illuminate\Database\Seeder;

class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->delete();

        $genres = [
            'Action',
            'Adventure',
            'Animation',
            'Biography',
            'Crime',
            'Comedy',
            'Documentary',
            'Drama',
            'Family',
            'Fantasy',
            'Film-Noir',
            'History',
            'Horror',
            'Music',
            'Musical',
            'Mystery',
            'Romance',
            'Sci-Fi',
            'Sport',
            'Thriller',
            'War'
        ];

        foreach ($genres as $value) {
            Genre::create([
             'name' => $value
            ]);
        }
    }
} 