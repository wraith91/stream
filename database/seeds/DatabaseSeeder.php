<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SentinelDatabaseSeeder::class);
        $this->call(GenreTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(YearTableSeeder::class);
        $this->call(JobRoleTableSeeder::class);
        $this->call(PeopleTableSeeder::class);
    }
}
