<?php

use App\Acme\Year;

use Illuminate\Database\Seeder;

class YearTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('years')->delete();

        $year = '1965';

        do {

            Year::create([
                'name' => $year
            ]);

            $year++;

        } while($year != '2017');


    }
}
