<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviePeopleRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_people_role', function (Blueprint $table) {
            $table->integer('movie_id')->unsigned()->index();
            $table->integer('people_id')->unsigned()->index();
            $table->integer('job_role_id')->unsigned()->index();

            $table->foreign('movie_id')
                ->references('id')
                ->on('movies')
                ->onDelete('cascade');

            $table->foreign('people_id')
                ->references('id')
                ->on('people')
                ->onDelete('cascade');

            $table->foreign('job_role_id')
                ->references('id')
                ->on('job_roles')
                ->onDelete('cascade');

            $table->primary(['movie_id', 'people_id', 'job_role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movie_people_role');
    }
}
