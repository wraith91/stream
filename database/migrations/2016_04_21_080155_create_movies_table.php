<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('year_id')->unsigned();
            $table->string('path');
            $table->mediumText('plot');
            $table->string('image');
            $table->string('title');
            $table->string('rating');
            $table->string('trailer');
            $table->string('duration', 10);
            $table->string('background')->nullable();
            $table->integer('view_count')->default(0);
            $table->timestamp('released_at');
            $table->timestamps();
            
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->foreign('country_id')
                    ->references('id')
                    ->on('countries')
                    ->onDelete('cascade');

            $table->foreign('year_id')
                    ->references('id')
                    ->on('years')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movies');
    }
}
