$('document').ready(function(){
    /* $('#search-input').attr('autocomplete', 'on');*/
    'use strict';

    $("#term").autocomplete({
        source: "{{ URL('/autocomplete') }}",
        type: "GET",
        minlength: 3,
        dataType: "json", 
    });
});

;( function( $ ) {
	$( '.swipebox-video' ).swipebox();
} )( jQuery );
$(document).ready(function(){
  $('.responsive').slick({
    dots: false,
    infinite: true,
    speed: 600,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});
//# sourceMappingURL=all.js.map
