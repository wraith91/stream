<?php

namespace App\Acme;

use Illuminate\Database\Eloquent\Model;

class MovieRolePeople extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'movie_people';

    public function movie()
	{
	  return $this->belongsTo(Movie::class);
	}

	public function jobRole()
	{
	  return $this->belongsTo(JobRole::class);
	}

	public function people()
	{
	  return $this->belongsTo(People::class);
	}

}
