<?php

namespace App\Acme;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'genres';
	
	/**
	 * Fillable fields for the model
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

    /**
     * Get the movies associated with the given genre
     * 
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'genre_movie');
    }

    /**
     * Scope a query to only filter the category
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */	
	public function scopeFilter($query, $filter)
	{
		return $query->where('name', $filter);
	}
}
