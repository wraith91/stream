<?php

namespace App\Acme;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'countries';
	
	/**
	 * Fillable fields for the model
	 *
	 * @var array
	 */
	protected $fillable = ['name'];
	
    /**
     * Each country belongs to a movie
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }    
}
