<?php

namespace App\Acme;

use Illuminate\Database\Eloquent\Model;

class JobRole extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_roles';
    
    /**
     * Fillable fields for the model
     *
     * @var array
     */
    protected $fillable = ['name']; 
    
    public function movieRolePeople()
    {
      return $this->hasMany(MovieRolePeople::class);
    }   	
       
    public function movie()
    {
        return $this->belongsToMany(Movie::class, 'movie_people_role');
    }
    
    public function people()
    {
        return $this->belongsToMany(People::class, 'movie_people_role');
    }
}
