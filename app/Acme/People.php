<?php

namespace App\Acme;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'people';
    
    /**
     * Fillable fields for the model
     *
     * @var array
     */
    protected $fillable = ['name'];
       
    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'cast_movie', 'cast_id', 'movie_id');
    }

    public function directedMovies()
    {
        return $this->belongsToMany(Movie::class, 'director_movie', 'director_id', 'movie_id');
    }
    
    public function jobRole()
    {
        return $this->belongsToMany(JobRole::class, 'movie_people_role');
    }

    public function scopeFilter($query, $actor)
    {
        return $query->where('name', $actor);
    }
    	
}
