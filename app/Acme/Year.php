<?php

namespace App\Acme;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'years';
	
	/**
	 * Fillable fields for the model
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

    /**
     * Each year belongs to a movie
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movies()
    {
        return $this->belongsTo(Movie::class, 'id', 'year_id');
    }

    /**
     * Scope a query to only filter the category
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */	
	public function scopeFilter($query, $filter)
	{
		return $query->where('name', $filter);
	}
}
