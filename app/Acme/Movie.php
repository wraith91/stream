<?php

namespace App\Acme;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'movies';
    
    /**
     * Fillable fields for the model
     *
     * @var array
     */
    protected $fillable = ['title', 'country_id', 'year_id', 'plot', 'rating', 'trailer', 'duration', 'image', 'path', 'background', 'view_count', 'released_at'];
  
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['released_at'];
        
    /**
     * Get First and Last name to get the full name
     * 
     * @param string
     */
    public function getFullNameAttribute() 
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    /**
     * Always set title name to uppercase every word when we save it to the db
     * 
     * @param string
     */
    public function setEmailAttribute($title)
    {
        $this->attributes['title'] = ucwords($title);
    }

    /**
     * Each movie is created by one user
     * 
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Each movie has one country
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    /**
     * Each movie has one year
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function year()
    {
        return $this->hasOne(Year::class, 'id', 'year_id');
    }

    /**
     * Each movie has many genres
     * 
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'genre_movie');
    }

    /**
     * Each movie belongs to a director
     * 
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function director()
    {
        return $this->belongsToMany(People::class, 'director_movie', 'movie_id', 'director_id');
    }

    /**
     * Each movie has many genres
     * 
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function cast()
    {
        return $this->belongsToMany(People::class, 'cast_movie', 'movie_id', 'cast_id');
    }

    /**
     * Get a list of tag ids associated with the current movie.
     * 
     * @return array
     */
    public function getGenreListAttribute()
    {
        return $this->genres->lists('id')->toArray();
    }
    
    /**
     * Get a list of director ids associated with the current movie.
     * 
     * @return array
     */
    public function getDirectorListAttribute()
    {
        return $this->director->lists('id')->toArray();
    }
    
    /**
     * Get a list of casts ids associated with the current movie.
     * 
     * @return array
     */
    public function getCastListAttribute()
    {
        return $this->cast->lists('id')->toArray();
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('title', 'LIKE', "%$search%");
    }

    public function scopeFilterCountry($query, $country)
    {
        return $query->whereHas('country', function ($query) use ($country) {
                        $query->where('name', $country);
                      });
    }

    public function scopeFilterYear($query, $year)
    {
        return $query->whereHas('year', function ($query) use ($year) {
                        $query->where('name', $year);
                      });
    }
}
