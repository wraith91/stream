<?php

namespace App\Providers;

use App\Acme\Year;
use App\Acme\Genre;
use App\Acme\Country;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.app.master', function($view) {
            $view->with('genres', Genre::lists('name', 'id'));
            $view->with('countries', Country::lists('name', 'id'));
            $view->with('years', Year::orderBy('name', 'desc')->lists('name', 'id'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
