<?php

Route::group(['as' => 'app::','middleware' => ['web']], function () {

    Route::get('/', ['as' => 'home', 'uses' => 'PagesController@index']);

    //Search Movie
    Route::get('/search', ['as' => 'movie.search', 'uses' => 'PagesController@search']);
    Route::get('/autocomplete', ['as' => 'movie.autocomplete', 'uses' => 'PagesController@autoComplete']);

    // Filter Movies by Genre, Year or Country
    Route::get('/browse-movies', ['as' => 'movie.filter.all', 'uses' => 'PagesController@filterAll']);
    Route::get('/year/{year}', ['as' => 'movie.filter.year', 'uses' => 'PagesController@filterYear']);
    Route::get('/genre/{genre}', ['as' => 'movie.filter.genre', 'uses' => 'PagesController@filterGenre']);
    Route::get('/country/{country}', ['as' => 'movie.filter.country', 'uses' => 'PagesController@filterCountry']);
    Route::get('/actor/{actor}', ['as' => 'movie.filter.actor', 'uses' => 'PagesController@filterActor']);
    Route::get('/director/{director}', ['as' => 'movie.filter.actor', 'uses' => 'PagesController@filterDirector']);

    // Show Movie
    Route::get('/movie/{title}', ['as' => 'movie.show', 'uses' => 'PagesController@show', 'middleware' => 'visits']);

});

// Route::get('/info', function ()
// {
//     $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.openload.co/1/']);
//     $response = $client->get('file/ul?login=47139f294035aefd&key=FDJwOwGl');

//     $decoded = json_decode($response->getBody(), true);

//     dd($decoded);
// });

Route::group(['middleware' => ['web']], function () {

    // Authorization
    Route::get('/login',  ['as' => 'auth.login.form', 'uses' => 'Auth\SessionController@getLogin']);
    Route::post('/login', ['as' => 'auth.login.attempt', 'uses' => 'Auth\SessionController@postLogin']);
    Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\SessionController@getLogout']);

    // Registration
    Route::get('register',  ['as' => 'auth.register.form', 'uses' => 'Auth\RegistrationController@getRegister']);
    Route::post('register', ['as' => 'auth.register.attempt', 'uses' => 'Auth\RegistrationController@postRegister']);

    // Activation
    Route::get('activate/{code}', ['as' => 'auth.activation.attempt', 'uses' => 'Auth\RegistrationController@getActivate']);
    Route::get('resend', ['as' => 'auth.activation.request', 'uses' => 'Auth\RegistrationController@getResend']);
    Route::post('resend', ['as' => 'auth.activation.resend', 'uses' => 'Auth\RegistrationController@postResend']);

    // Password Reset
    Route::get('password/reset/{code}', ['as' => 'auth.password.reset.form', 'uses' => 'Auth\PasswordController@getReset']);
    Route::post('password/reset/{code}', ['as' => 'auth.password.reset.attempt', 'uses' => 'Auth\PasswordController@postReset']);
    Route::get('password/reset', ['as' => 'auth.password.request.form', 'uses' => 'Auth\PasswordController@getRequest']);
    Route::post('password/reset', ['as' => 'auth.password.request.attempt', 'uses' => 'Auth\PasswordController@postRequest']);

    // Users
    Route::resource('users', 'UserController');

    // Roles
    Route::resource('roles', 'RoleController');

    // Dashboard
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => function() {
        return view('centaur.dashboard');
    }]);

    Route::group(['middleware' => ['auth']], function () {
        
        // Movies
        Route::resource('movies', 'MovieController');
        Route::get('movies/search/autocomplete', 'MovieController@autoComplete');

        //Casts
        Route::resource('casts', 'CastController');
        Route::get('casts/create/autocomplete', 'CastController@autoComplete');
    });
});