<?php

namespace App\Http\Middleware;

use App\Acme\Movie;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Session\Store;

class CountMovieVisits
{
    /**
     * @var Store
     */
    protected $session;
    /**
     * @var Movie
     */
    protected $movie;

    /**
     * @param Store $session
     * @param Movie $movie
     */
    function __construct(Store $session, Movie $movie)
    {
        $this->session = $session;
        $this->movie = $movie;
    }

    /**
     * Handle an incoming request.
     * Checks if we are on page for showing single resource and
     * increment its visit number if timestamp is later than 2 hours.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return Response
     */
    public function handle($request, Closure $next)
    {
        $title = substr(\Request::path(), 6);

        $slug = str_replace('_', ' ', $title);

        $timestamp = $this->session->get('visits.' . $slug);

         if ( ! $this->isRecentVisit($timestamp))
         {
             $mo = $this->movie->search($slug)->first()->increment('view_count');
             $this->session->put('visits.' . $slug, Carbon::now());
         }

        return $next($request);
    }

    /**
     * Checks if timestamp is earlier than now minus 2 hours.
     *
     * @param Carbon $timestamp
     * @return bool
     */
    private function isRecentVisit($timestamp)
    {
        $earlierTimestamp = Carbon::now()->subHours(2);

        return ! is_null($timestamp) && $timestamp->gt($earlierTimestamp);
    }
}


    


