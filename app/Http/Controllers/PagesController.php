<?php

namespace App\Http\Controllers;

use App\Acme\Year;
use App\Acme\Genre;
use App\Acme\Movie;
use App\Acme\People;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PagesController extends Controller
{
  protected $movie;
  protected $genre;
  protected $people;

  public function __construct(Movie $movie, Genre $genre, People $people)
  {
    $this->movie = $movie;
    $this->genre = $genre;
    $this->people = $people;
  }

	public function index()
    {
      $movie = $this->movie->max('view_count');

      $latest_movies = $this->movie->orderBy('id', 'desc')->limit(10)->get();
           
      $movie_by_year =  $this->movie->with('year')->orderBy('year_id', 'desc')->paginate(12);

    	return view('app.index', compact('latest_movies', 'movie_by_year'));
    }

    public function show($title)
    {
        $query = str_replace('_', ' ', $title);

        $movie = $this->movie->with('year', 'country')->where('title', 'LIKE', "%$query%")->first();

        $recommended_movies = $this->movie->with('year', 'country')
                                    ->whereHas('cast', function ($query) use ($movie) {
                                        $query->where('name', $movie->cast[rand(0, 3)]->name);
                                    })
                                    ->limit(4)->get();

        return view('app.movies.show', compact('movie', 'recommended_movies'));
    }

   public function search(Request $request)
   {
   		$this->validate($request, ['term' => 'required']);

   		$query = $request->get('term');

   		$results = $query 
   					? $this->movie->search($query)->paginate(5)
   					: $this->movie->paginate(20);
   
   		return view('app.movies.search', compact('results'));
   }

    public function autoComplete()
    {
        $term = trim(Input::get('term'));

        $results = $this->movie->where('title', 'LIKE', "%$term%")->get();

        foreach ($results as $item) {
            $data[] = $item->title;
        }

        return response()->json($data);
    }

   public function filterAll()
   {
      $genre = \Request::get('genre');
      $year = \Request::get('year');
      $country = \Request::get('country');

      $results = $this->movie;
 
      if ($year)
      {
        $results = $results->where('year_id', $year);
      }                       
      
      if ($country)
      {
        $results = $results->where('country_id', $country);
      }

      if ($genre)
      {
        $results = $results->whereHas('genres', function ($query) use ($genre) {
                            $query->where('id', $genre);
                          });
      }
                     
      $results = $results->paginate(5);
 
      return view('app.movies.search', compact('results'));
   }

   public function filterActor($actor)
   {
      $movies = $this->people->with('movies')->filter($actor)->get();

      return view('app.movies.filter_actor', compact('movies'));
   }  

   public function filterDirector($director)
   {
      $movies = $this->people->with('directedMovies')->filter($director)->get();

      return view('app.movies.filter_director', compact('movies'));
   }  

   public function filterGenre($genre)
   {
      $movies = $this->genre->with('movies')->filter($genre)->get();

      return view('app.movies.filter_genre', compact('movies'));
   }

   public function filterYear($year)
   {
      $movies = $this->movie->filterYear($year)->get();

      return view('app.movies.filter_year', compact('movies', 'year'));
   }

   public function filterCountry($country)
   {
      $movies = $this->movie->filterCountry($country)->get();

      return view('app.movies.filter_country', compact('movies', 'country'));
   }
}
