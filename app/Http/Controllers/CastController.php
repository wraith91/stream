<?php

namespace App\Http\Controllers;

use App\Acme\People;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;

class CastController extends Controller
{
    protected $people;

    public function __construct(People $people)
    {
        $this->people = $people;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $people = $this->people->paginate(10);

        return view('admin.casts.index', compact('people'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.casts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $cast = People::create(['name' => trim(ucwords($request->get('term')))]);
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withErrors('Duplicate Entry!');
        }

        session()->flash('flash_message', $cast->name . ' was added.');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = People::find($id);

        return view('admin.casts.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cast = People::findOrFail($id);

        $cast->update($request->only('name'));

        session()->flash('flash_message', 'Cast name has been modified!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = People::find($id);

        $cast->delete();

        session()->flash('flash_message', $cast->name . ' was removed.');

        return redirect()->back();
    }

    public function autoComplete()
    {
        $term = trim(Input::get('term'));

        $results = People::where('name', 'LIKE', "%$term%")->get();

        foreach ($results as $item) {
            $data[] = $item->name;
        }

        return response()->json($data);
    }
}
