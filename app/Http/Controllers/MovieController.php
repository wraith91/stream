<?php

namespace App\Http\Controllers;

use App\Acme\User;
use App\Acme\Genre;
use App\Acme\Year;
use App\Acme\Movie;
use App\Acme\People;
use App\Acme\JobRole;
use App\Acme\Country;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\MovieUploadRequest;

class MovieController extends Controller
{
    protected $user;
    protected $genre;
    protected $movie;
    protected $year;
    protected $country;

    public function __construct(Genre $genre, Movie $movie, Year $year, Country $country)
    {
        $this->user = User::find(\Sentinel::getUser()->id );
        $this->year = $year;
        $this->genre = $genre;
        $this->movie = $movie;
        $this->country = $country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = strtolower(Input::get('term'));

        $movies = $this->movie->orderBy('id','desc')->paginate(10);
        
        if ($title) {
            $movies = $this->movie->where('title', 'LIKE', "%$title%")->orderBy('year_id', 'asc')->paginate(5);
        }

        return view('admin.movies.index', compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = $this->genre->lists('name', 'id');

        $countries = $this->country->lists('name', 'id');

        $years = $this->year->orderBy('name', 'desc')->lists('name', 'id');

        $people = People::lists('name', 'id');

        return view('admin.movies.create',compact('genres', 'countries', 'years', 'job_roles', 'people'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MovieUploadRequest $request)
    {
        $inputs = $request->except(['_token', 'image', 'genre', 'background', 'director']);

        if($request->hasfile('image'))
        {
            $name = $this->checkUpload($request->file('image'), 'posters');

            $inputs = $inputs + ['image' => $name];
        }

        if($request->hasfile('background'))
        {
            $name = $this->checkUpload($request->file('background'), 'background');

            $inputs = $inputs + ['background' => $name];
        }

        $movie = $this->user->movie()->create($inputs);

        $movie->genres()->attach($request->input('genre'));
        
        $movie->director()->attach($request->input('director'));

        $movie->cast()->attach($request->input('cast'));

        session()->flash('flash_message', $movie->title . ' has been successfully uploaded!');
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genres = $this->genre->lists('name', 'id');

        $countries = $this->country->lists('name', 'id');

        $years = $this->year->orderBy('name', 'desc')->lists('name', 'id');

        $movie = $this->movie->findOrfail($id);

        $people = People::lists('name', 'id');

        return view('admin.movies.edit',compact('genres', 'countries', 'years', 'movie', 'people'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $movie = $this->movie->find($id);

        $inputs = $request->except(['_token', '_method', 'image', 'background']);

        if($request->hasfile('image'))
        {
            $imageName = $this->checkUpload($request->file('image'), 'posters');

            $inputs = $inputs + ['image' => $imageName];
        }

        if($request->hasfile('background'))
        {
            $bgName = $this->checkUpload($request->file('background'), 'background');

            $inputs = $inputs + ['background' => $bgName];
        }

        $movie->genres()->sync($request->input('genre_list'));
       
        $movie->director()->sync($request->input('director_list'));

        $movie->cast()->sync($request->input('cast_list'));

        if($movie->fill($inputs)->save())
        {
            session()->flash('flash_message', $movie->title . ' has been successfully modified!');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = $this->movie->find($id);

        if($movie->delete())
        {
            session()->flash('flash_message', $movie->title . ' has been deleted!');
        }

        return redirect()->back();
    }

    public function autoComplete()
    {
        $term = trim(Input::get('term'));

        $results = $this->movie->where('title', 'LIKE', "%$term%")->get();

        foreach ($results as $item) {
            $data[] = $item->title;
        }

        return response()->json($data);
    }

    /**
     * Check if the file has been uploaded and moving the file to its directory.
     *
     * @param  $string, $string
     * @return  $string
     */
    public function checkUpload($name, $folder)
    {       
        $file = $name;

        if ($file->isValid($name))
        {
            $fileName = time() . '-' . $file->getClientOriginalName();

            $path = $file->move(public_path().'/main/images/upload/movie/'. $folder . '/', $fileName);      

            return $fileName;    
        }

        return redirect()->back()->withErrors('Invalid File');
    }
}
