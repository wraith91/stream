<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MovieUploadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $input['title']  = trim($input['title']);
        $input['rating'] = trim($input['rating']);
        $input['trailer'] = trim($input['trailer']);
        $input['duration'] = trim($input['duration']);
        $this->replace($input);

        return [
            'title'      => 'required',
            'genre'      => 'required',
            'country_id' => 'required',
            'year_id'    => 'required',
            'plot'       => 'required',
            'rating'     => 'required',
            'duration'   => 'required',
            'image'      => 'required',
            'background' => 'required',
            'trailer'    => 'required',
            'director'   => 'required',
            'cast'       => 'required',
            'path'       => 'required',
        ];
    }
}
