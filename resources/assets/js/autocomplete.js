$('document').ready(function(){
    /* $('#search-input').attr('autocomplete', 'on');*/
    'use strict';

    $("#term").autocomplete({
        source: "{{ URL('/autocomplete') }}",
        type: "GET",
        minlength: 3,
        dataType: "json", 
    });
});
