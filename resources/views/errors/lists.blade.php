@if ($errors->any())
<div class="alert alert-danger" role="alert">
  <!-- <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> -->
  <span class="sr-only">Errors:</span>
  <ul>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
  </ul>
</div>
@endif