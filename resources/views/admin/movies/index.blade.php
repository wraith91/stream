@extends('Centaur::layout')

@section('title', 'Movies')

@section('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" type="text/css" />
@stop

@section('content')
    
    @include('partials.flash')
    
    {!! Form::open(['method' => 'GET', 'action' => 'MovieController@index']) !!}
        {!! Form::text('term', null, ['id' => 'search-input', 'class' => 'form-control', 'placeholder' => 'Search movie archives...', 'tabindex' => 1]) !!}
    {!! Form::close() !!}    
    
    <div class="page-header">
        <div class='btn-toolbar pull-right'>
            <a class="btn btn-primary btn-lg" href="{{ url('movies/create') }}">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                Upload Movie
            </a>
        </div>
        <h1>Movies</h1>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>Plot</th>
                            <th>Image</th>
                            <th>Uploaded Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($movies as $i => $movie)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>{{ str_limit($movie->title, 30) }}</td>
                                <td>{{ str_limit($movie->plot, 70) }}</td>
                                <td><img src="{{ asset('main/images/upload/movie/posters/' . $movie->image) }}" alt="{{ $movie->title }}" width="45" height="60"></td>
                                <td>{{ $movie->created_at->diffForHumans() }}</td>
                                <td>
                                    <a href="{{ route('movies.edit', $movie->id) }}" class="btn btn-default">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        Edit
                                    </a>
                                    <a href="{{ route('movies.destroy', $movie->id) }}" class="btn btn-danger" data-method="delete" data-token="{{ csrf_token() }}">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $movies->appends(Request::except('page'))->render() !!}                
            </div>
        </div>
    </div>
@stop

@section('footer')  
   <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script>
        $('document').ready(function(){
            /* $('#search-input').attr('autocomplete', 'on');*/
            'use strict';

            $("#search-input").autocomplete({
            source: "{{ URL('movies/search/autocomplete') }}",
            type: "GET",
            minlength: 3,
            dataType: "json", 
                });
            });
    </script>
@stop
