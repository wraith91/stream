@extends('Centaur::layout')

@section('title', 'Upload Movie')

@section('content')
<div class="page-header">
    <h1>Upload Movie</h1>
</div>
<div class="row">
    @include('partials.flash')
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body"> 
                {!! Form::open(['route' => 'movies.store', 'files' => true]) !!}
                <fieldset>
                    <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
                        {!! Form::label('title', 'Title') !!}
                        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Movie Title']) !!}
                        {!! ($errors->has('title') ? $errors->first('title', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('genre')) ? 'has-error' : '' }}">
                        {!! Form::label('genre', 'Genre') !!}
                        {!! Form::select('genre[]', $genres, null, ['id' => 'genre_list', 'class' => 'form-control', 'multiple']) !!}
                        {!! ($errors->has('genre') ? $errors->first('genre', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('country_id')) ? 'has-error' : '' }}">
                        {!! Form::label('country_id', 'Country') !!}
                        {!! Form::select('country_id', $countries, null, ['id' => 'country_list', 'class' => 'multiple form-control', 'placeholder' => '']) !!}
                        {!! ($errors->has('country_id') ? $errors->first('country_id', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('year_id')) ? 'has-error' : '' }}">
                        {!! Form::label('year_id', 'Year') !!}
                        {!! Form::select('year_id', $years, null, ['id' => 'year_list', 'class' => 'form-control', 'placeholder' => '']) !!}
                        {!! ($errors->has('year_id') ? $errors->first('year_id', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('plot')) ? 'has-error' : '' }}">
                        {!! Form::label('plot', 'Plot') !!}
                        {!! Form::textarea('plot', null, ['class' => 'form-control', 'placeholder' => 'Movie Plot']) !!}
                        {!! ($errors->has('plot') ? $errors->first('plot', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('rating')) ? 'has-error' : '' }}">
                        {!! Form::label('rating', 'Rating') !!}
                        {!! Form::text('rating', null, ['class' => 'form-control', 'placeholder' => 'Movie Rating']) !!}
                        {!! ($errors->has('rating') ? $errors->first('rating', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('duration')) ? 'has-error' : '' }}">
                        {!! Form::label('duration', 'Duration') !!}
                        {!! Form::text('duration', null, ['class' => 'form-control', 'placeholder' => 'Movie Duration']) !!}
                        {!! ($errors->has('duration') ? $errors->first('duration', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <fieldset>                    
                    <div class="form-group {{ ($errors->has('director')) ? 'has-error' : '' }}">
                        {!! Form::label('director', 'Director') !!}
                        {!! Form::select('director', $people, null, ['id' => 'director', 'class' => 'form-control', 'placeholder' => '']) !!}
                        {!! ($errors->has('director') ? $errors->first('director', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('cast')) ? 'has-error' : '' }}">
                        {!! Form::label('cast', 'Cast') !!}
                        {!! Form::select('cast[]', $people, null, ['id' => 'cast', 'class' => 'form-control', 'multiple']) !!}
                        {!! ($errors->has('cast') ? $errors->first('cast', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('image')) ? 'has-error' : '' }}">
                        {!! Form::label('image', 'Upload Image') !!}
                        {!! Form::file('image') !!}
                        <p class="help-block">Image Size: 175 x 260</p>
                        {!! ($errors->has('image') ? $errors->first('image', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('background')) ? 'has-error' : '' }}">
                        {!! Form::label('background', 'Upload Background Image') !!}
                        {!! Form::file('background') !!}
                        {!! ($errors->has('background') ? $errors->first('background', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('trailer')) ? 'has-error' : '' }}">
                        {!! Form::label('trailer', 'Trailer') !!}
                        {!! Form::text('trailer', null, ['class' => 'form-control', 'placeholder' => 'Movie Trailer: Url of the trailer']) !!}
                        {!! ($errors->has('trailer') ? $errors->first('trailer', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('path')) ? 'has-error' : '' }}">
                        {!! Form::label('path', 'Upload Movie') !!}
                        {!! Form::text('path', null, ['class' => 'form-control', 'placeholder' => 'Movie URL']) !!}
                        {!! ($errors->has('path') ? $errors->first('path', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Upload', ['class' => 'btn btn-lg btn-primary btn-block']) !!}
                    </div>
                </fieldset>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
    <script>
        $('#genre_list').select2({
          placeholder: "Select a genres",
          allowClear: true
        });
        $('#year_list').select2({
          placeholder: "Select Year...",
          allowClear: true
        });
        $('#country_list').select2({
          placeholder: "Select Country...",
          allowClear: true
        });
        $('#director').select2({
          placeholder: "Select director",
          allowClear: true
        });
        $('#cast').select2({
          placeholder: "Select casts",
          allowClear: true
        });
    </script>
@stop
