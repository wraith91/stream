@extends('Centaur::layout')

@section('title', 'Movies')

@section('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" type="text/css" />
@stop

@section('content')
    
    @include('partials.flash')
    
    @include('errors.lists')
    
    <div class="page-header">
        <h1>Insert Casts</h1>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-md-offset-3">
            {!! Form::model($cast, ['method' => 'PUT', 'route' => ['casts.update', $cast->id]]) !!}
        
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', null, ['id' => 'search-input','class' => 'form-control', 'tabindex' => 1]) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Update', ['class' => 'btn btn-lg btn-primary btn-block']) !!}
                </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop