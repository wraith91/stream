@extends('Centaur::layout')

@section('title', 'Movies')

@section('content')
    
    @include('partials.flash')
    
    <div class="page-header">
        <div class='btn-toolbar pull-right'>
            <a class="btn btn-primary btn-lg" href="{{ url('casts/create') }}">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                New Cast
            </a>
        </div>
        <h1>Casts</h1>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Created Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($people as $i => $cast)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>{{ $cast->name }}</td>
                                <td>{{ $cast->created_at->diffForHumans() }}</td>
                                <td>
                                    <a href="{{ route('casts.edit', $cast->id) }}" class="btn btn-default">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        Edit
                                    </a>
                                    <a href="{{ route('casts.destroy', $cast->id) }}" class="btn btn-danger" data-method="delete" data-token="{{ csrf_token() }}">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $people->render() !!}
            </div>
        </div>
    </div>
@stop
