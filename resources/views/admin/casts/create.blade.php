@extends('Centaur::layout')

@section('title', 'Movies')

@section('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" type="text/css" />
@stop

@section('content')
    
    @include('partials.flash')
    
    @include('errors.lists')
    
    <div class="page-header">
        <h1>Insert Casts</h1>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-md-offset-3">
            {!! Form::open(['route' => 'casts.store']) !!}
        
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('term', null, ['id' => 'search-input','class' => 'form-control', 'tabindex' => 1]) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Save', ['class' => 'btn btn-lg btn-primary btn-block']) !!}
                </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('footer')  
   <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script> -->
    <script>

        $('document').ready(function(){
            /* $('#search-input').attr('autocomplete', 'on');*/
            'use strict';

            $("#search-input").autocomplete({
            source: "{{ URL('casts/create/autocomplete') }}",
            type: "GET",
            minlength: 3,
            dataType: "json", 
                });
            });
    </script>
@stop
