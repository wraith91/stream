@if (Session::has('flash_message'))
<div class="alert alert-success" role="alert">
  <p>{!! Session::get('flash_message') !!}</p>
</div>
@endif