<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="Free Movie Streaming Site">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title', 'Stream')</title>

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{ asset('main/css/app.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" type="text/css" />

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="container">
            <header>
                <div class="inner-header">
                    <h1 class="logo"><a href="/">Stream2k</a></h1>
                    
                    @include('layouts.app.menu')
                    
                </div>
            </header> <!-- End of Header  -->
            <main>
                <div class="highlights">
                    @yield('highlights')
                </div>
                <div class="inner-main">
                  <div class="contents">
                      @yield('contents')
                  </div>
                  <div class="sidebar-right">
                      <div class="inner-contents">
                        <div class="section-title">
                          <h3><strong>Filter</strong> Movie</h3>
                        </div>
                        {!! Form::open(['method' => 'GET', 'action' => 'PagesController@filterAll']) !!}
                            {!! Form::select('genre', $genres, Request::get('genre'),['class' => 'form-select', 'placeholder' => 'Select a Genre...']) !!}
                            {!! Form::select('country', $countries, Request::get('country'),['class' => 'form-select', 'placeholder' => 'Select a Country...']) !!}
                            {!! Form::select('year', $years, Request::get('year'),['class' => 'form-select', 'placeholder' => 'Select a Year...']) !!}
                            {!! Form::submit('Filter Movie', ['class' => 'btn']) !!}
                        {!! Form::close() !!}
                      </div>
                  </div>
                </div>
            </main>
            <footer>
                @include('layouts.app.footer')
            </footer>
        </div>

        <div id="fb-root"></div>

        <script>
            (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <script src="{{ asset('main/js/jquery.min.js') }}"></script>
        <script src="{{ asset('main/js/jquery.swipebox.js') }}"></script>
        <script src="{{ asset('main/js/slick.js') }}"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="{{ asset('main/js/dist/all.js') }}"></script>
        <script src="{{ asset('main/js/jquery.infinitescroll.min.js') }}"></script>
        <script src="{{ asset('main/js/masonry.pkgd.min.js') }}"></script>

        @yield('footer')
        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<!--         <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script> -->
        <!-- <script src="https://www.google-analytics.com/analytics.js" async defer></script> -->
    </body>
</html>