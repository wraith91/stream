<div class="inner-footer">
  <div class="sub-nav-block">                      
    <div class="section-title">
      <h4>Genre</h4>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="{{ URL::to('/genre/action') }}">Action</a></li>
        <li><a href="{{ URL::to('/genre/adventure') }}">Adventure</a></li>
        <li><a href="{{ URL::to('/genre/animation') }}">Animation</a></li>
        <li><a href="{{ URL::to('/genre/biography') }}">Biography</a></li>
        <li><a href="{{ URL::to('/genre/crime') }}">Crime</a></li>
        <li><a href="{{ URL::to('/genre/comedy') }}">Comedy</a></li>
        <li><a href="{{ URL::to('/genre/documentary') }}">Documentary</a></li>
        <li><a href="{{ URL::to('/genre/drama') }}">Drama</a></li>
      </ul>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="{{ URL::to('/genre/family') }}">Family</a></li>
        <li><a href="{{ URL::to('/genre/fantasy') }}">Fantasy</a></li>
        <li><a href="{{ URL::to('/genre/film-Noir') }}">Film-Noir</a></li>
        <li><a href="{{ URL::to('/genre/history') }}">History</a></li>
        <li><a href="{{ URL::to('/genre/horror') }}">Horror</a></li>
        <li><a href="{{ URL::to('/genre/music') }}">Music</a></li>
        <li><a href="{{ URL::to('/genre/musical') }}">Musical</a></li>
        <li><a href="{{ URL::to('/genre/mystery') }}">Mystery</a></li>
      </ul>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="{{ URL::to('/genre/romance') }}">Romance</a></li>
        <li><a href="{{ URL::to('/genre/sci-fi') }}">Sci-Fi</a></li>
        <li><a href="{{ URL::to('/genre/sport') }}">Sport</a></li>
        <li><a href="{{ URL::to('/genre/thriller') }}">Thriller</a></li>
        <li><a href="{{ URL::to('/genre/war') }}">War</a></li>
      </ul>
    </div>
  </div>
  <div class="sub-nav-block">                      
    <div class="section-title">
      <h4>Country</h4>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="{{ URL::to('/country/amerika') }}">Amerika</a></li>
        <li><a href="{{ URL::to('/country/australia') }}">Australia</a></li>
        <li><a href="{{ URL::to('/country/china') }}">China</a></li>
        <li><a href="{{ URL::to('/country/perancis') }}">Perancis</a></li>
        <li><a href="{{ URL::to('/country/jerman') }}">Jerman</a></li>
        <li><a href="{{ URL::to('/country/hongkong') }}">Hongkong</a></li>
        <li><a href="{{ URL::to('/country/india') }}">India</a></li>
        <li><a href="{{ URL::to('/country/inggris') }}">Inggris</a></li>
      </ul>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="{{ URL::to('/country/iran') }}">Iran</a></li>
        <li><a href="{{ URL::to('/country/israel') }}">Israel</a></li>
        <li><a href="{{ URL::to('/country/itali') }}">Itali</a></li>
        <li><a href="{{ URL::to('/country/jepang') }}">Jepang</a></li>
        <li><a href="{{ URL::to('/country/kanada') }}">Kanada</a></li>
        <li><a href="{{ URL::to('/country/korea') }}">Korea</a></li>
        <li><a href="{{ URL::to('/country/malaysia') }}">Malaysia</a></li>
        <li><a href="{{ URL::to('/country/meksiko') }}">Meksiko</a></li>
      </ul>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="{{ URL::to('/country/pilipina') }}">Pilipina</a></li>
        <li><a href="{{ URL::to('/country/romania') }}">Romania</a></li>
        <li><a href="{{ URL::to('/country/rusia') }}">Rusia</a></li>
        <li><a href="{{ URL::to('/country/taiwan') }}">Taiwan</a></li>
        <li><a href="{{ URL::to('/country/tailand') }}">Tailand</a></li>
      </ul>
    </div>
  </div>
  <div class="sub-nav-block">                      
    <div class="section-title">
      <h4>Year</h4>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="{{ URL::to('/year/1997') }}">1997</a></li>
        <li><a href="{{ URL::to('/year/1998') }}">1998</a></li>
        <li><a href="{{ URL::to('/year/1999') }}">1999</a></li>
        <li><a href="{{ URL::to('/year/2000') }}">2000</a></li>
        <li><a href="{{ URL::to('/year/2001') }}">2001</a></li>
        <li><a href="{{ URL::to('/year/2002') }}">2002</a></li>
        <li><a href="{{ URL::to('/year/2003') }}">2003</a></li>
        <li><a href="{{ URL::to('/year/2004') }}">2004</a></li>
      </ul>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="{{ URL::to('/year/2005') }}">2005</a></li>
        <li><a href="{{ URL::to('/year/2006') }}">2006</a></li>
        <li><a href="{{ URL::to('/year/2007') }}">2007</a></li>
        <li><a href="{{ URL::to('/year/2008') }}">2008</a></li>
        <li><a href="{{ URL::to('/year/2009') }}">2009</a></li>
        <li><a href="{{ URL::to('/year/2010') }}">2010</a></li>
        <li><a href="{{ URL::to('/year/2012') }}">2012</a></li>
        <li><a href="{{ URL::to('/year/2013') }}">2013</a></li>
      </ul>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="{{ URL::to('/year/2014') }}">2014</a></li>
        <li><a href="{{ URL::to('/year/2015') }}">2015</a></li>
        <li><a href="{{ URL::to('/year/2016') }}">2016</a></li>
      </ul>
    </div>
  </div>
  <div class="sub-nav-block">                      
    <div class="section-title">
      <h4>Others</h4>
    </div>
    <div class="nav-lists">
      <ul>
        <li><a href="#">Browse Movies</a></li>
        <li><a href="#">DMCA</a></li>
        <li><a href="#">Request</a></li>
        <li><a href="#">Contact US</a></li>
      </ul>
    </div>
  </div>
</div>
<div class="copyright">
  <strong>&copy Stream2k 2016. All rights reserved.</strong>
</div>