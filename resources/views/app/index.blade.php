@extends('layouts.app.master')

@section('highlights')
	<section class="latest-movies">
		<div class="inner-contents">
			<div class="section-title">
				<h3><strong>Latest</strong> Movies</h3>
			</div>
			<div class="movie-grid responsive">
				@foreach($latest_movies as $movie)
					<div class="movie-item">
						<figure class="effect-chico">
							<img src="{{ asset('/main/images/upload/movie/posters/' . $movie->image) }}" alt="img15"/>
							<figcaption>
								<h2>{{ $movie->rating }} <span>rating</span></h2>
								<a href="movie/{{ strtolower(preg_replace('/\s+/', '_', $movie->title)) }}" class="play-btn">Play</a>
								<a href="{{ $movie->trailer }}" class="swipebox-video trailer-btn" rel="youtube">Trailer</a>
								<a href="#" class="overlay">View more</a>
							</figcaption>			
						</figure>
						<div class="movie-title">
							<h4>{{ $movie->title }}</h4>
						</div>
					</div> <!-- End of 1 Movie Item  -->
				@endforeach
			</div>
		</div>
	</section>
@stop

@section('contents')
	<section class="filtered-movies">
		<div class="inner-contents">
			<div id="movies" class="movie-lists movie-grid">
				<div class="section-title">
					<h3><strong>Movies</strong> by year</h3>
				</div>
				@foreach ($movie_by_year as $m)
					<div class="movie-item">
						<figure class="effect-chico">
							<img src="{{ asset('/main/images/upload/movie/posters/' . $m->image) }}" alt="{{ $m->title }}"/>
							<figcaption>
								<h2>{{ $m->rating }} <span>rating</span></h2>
								<a href="movie/{{ strtolower(preg_replace('/\s+/', '_', $m->title)) }}" class="play-btn" data-method="get"></a>
								<a href="{{ $m->trailer }}" class="swipebox-video trailer-btn" rel="youtube">Trailer</a>
								<a href="#" class="overlay">View more</a>
							</figcaption>		
						</figure>
						<div class="movie-title">
							<h4>{{ $m->title }}</h4>
							<ul>
							@foreach ($m->genres as $genre)
								<li><a href="{{ URL::to('/genre/' . strtolower($genre->name)) }}">{{ $genre->name }}</a></li>
							@endforeach
							</ul>
						</div>
					</div>		
				@endforeach
			</div>
		</div>
	</section>
    {{ $movie_by_year->render() }}
@stop

@section('footer')
    <script>
    	$(document).ready(function(){
    		var loading_options = {
                finishedMsg: "<div class='end-msg'>Congratulations! You've reached the end of the internet</div>",
                msgText: "<div class='center'>Loading news items...</div>",
                img: "../main/images/ajax-loader.gif"
            };

            $('#movies').infinitescroll({
                loading : loading_options,
                navSelector : "ul.pagination",
                nextSelector : "ul.pagination li.active + li a",
                itemSelector : "#movies .movie-item"
            });		
		});
    </script>
@stop