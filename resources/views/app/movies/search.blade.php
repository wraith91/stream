@extends('layouts.app.master')

@section('contents')
	<section class="filtered-movies">
		<div class="inner-contents">
			<div class="movie-lists movie-grid">
				<div class="section-title">
					<h3><strong>Search Results: </strong>{{ ucwords(Request::get('movie'))	}}</h3>
				</div>
				
				@if ($results->count())
					@foreach ($results as $item)
						<div class="searched-result-item">
							<figure class="movie-image">
								<img src="{{ asset('/main/images/upload/movie/posters/' . $item->image) }}" alt="{{ $item->title }}"/>		
							</figure>
							<div class="movie-details">
								<h3><a href="movie/{{ strtolower(preg_replace('/\s+/', '_', $item->title)) }}">{{ $item->title . ' (' . $item->year->name . ')' }}</a></h3>
								<ul>
									@foreach ($item->genres as $genre)
										<li><a href="{{ URL::to('/genre/' . strtolower($genre->name)) }}">{{ $genre->name }}</a></li>
									@endforeach
								</ul>
								<p>{{ str_limit($item->plot, 350) }}</p>
							</div>
						</div> <!-- End of 1 Movie Item  -->
					@endforeach
				@else 
					<p>No Record was found...</p>
				@endif
				<!-- Content Should be placed here! -->
			</div>
		</div>
		{!! $results->appends(Request::except('page'))->render() !!}
	</section>
@stop
