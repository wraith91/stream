@extends('layouts.app.master')

@section('highlights')
	<section class="movie-player-poster">
		<div class="inner-contents">
			<div class="movie-poster">
				<figure><img src="{{ asset('main/images/upload/movie/posters/' . $movie->image) }}" alt="{{ $movie->title }}"></figure>
			</div>
			<div class="movie-player">
				<iframe src="{{ $movie->path }}" scrolling="no" frameborder="0" width="760" height="435" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>
			</div>
			<div id="background-image" class="background-image" style="background: url({{ asset('main/images/upload/movie/background/' . $movie->background) }}) no-repeat center center; background-size: cover; -webkit-background-size: cover;-moz-background-size: cover; -o-background-size: cover;"></div>
			<div class="background-overlay"></div>
		</div>
	</section>
@stop

@section('contents')
	<section class="filtered-movies">
		<div class="inner-contents">
			<div class="movie-lists movie-grid">
				<div class="section-title">
					<h3><strong>Recommended</strong> Movies</h3>
				</div>
				@foreach ($recommended_movies as $film)
					<div class="movie-item">
						<figure class="effect-chico">
							<img src="{{ asset('/main/images/upload/movie/posters/' . $film->image) }}" alt="{{ $film->title }}"/>
							<figcaption>
								<h2>{{ $film->rating }} <span>rating</span></h2>
								<a href="{{ URL::to('movie/' . strtolower(preg_replace('/\s+/', '_', $film->title)) )}}" class="play-btn" data-method="get"></a>
								<a href="{{ $film->trailer }}" class="swipebox-video trailer-btn" rel="youtube">Trailer</a>
								<a href="#" class="overlay">View more</a>
							</figcaption>		
						</figure>
						<div class="movie-title">
							<h4>{{ $film->title }}</h4>
							<ul>
							@foreach ($film->genres as $genre)
								<li><a href="{{ URL::to('/genre/' . strtolower($genre->name)) }}">{{ $genre->name }}</a></li>
							@endforeach
							</ul>
						</div>
					</div>		
				@endforeach
			</div>
		</div>
	</section>
	<section class="movie-details">
		<div class="inner-contents">
			<div class="section-title">
				<h3><strong>Synopsis</strong></h3>
			</div>
			<p>{{ $movie->plot }}</p>
			<div class="movie-detail-blocks">
				<div class="section-title">
					<h3><strong>Director</strong></h3>
				</div>
				<ul>
					@foreach ($movie->director as $director)
						<li><a href="{{ URL::to('/director/' . strtolower($director->name)) }}">{{ $director->name }}</a></li>
					@endforeach
				</ul>
			</div>
			<div class="movie-detail-blocks">
				<div class="section-title">
					<h3><strong>Casts</strong></h3>
				</div>
				<ul>
					@foreach ($movie->cast as $cast)
						<li><a href="{{ URL::to('/actor/' . strtolower($cast->name)) }}">{{ $cast->name }}</a></li>
					@endforeach
				</ul>
			</div>
			<div class="movie-detail-blocks">
				<div class="section-title">
					<h3><strong>Genre</strong></h3>
				</div>
				<ul>
					@foreach ($movie->genres as $genre)
						<li><a href="{{ URL::to('/genre/' . strtolower($genre->name)) }}">{{ $genre->name }}</a></li>
					@endforeach
				</ul>
			</div>
			<div class="movie-detail-blocks">
				<div class="section-title">
					<h3><strong>Country</strong></h3>
				</div>
				<p>{{ $movie->country->name }}</p>					
			</div>
			<div class="movie-detail-blocks">
				<div class="section-title">
					<h3><strong>Ratings</strong></h3>
				</div>
				<p>{{ $movie->rating }}</p>
			</div>
			<div class="movie-detail-blocks">
				<div class="section-title">
					<h3><strong>Duration</strong></h3>
				</div>
				<p>{{ $movie->duration }}</p>
			</div>
		</div>
	</section>
	<section class="movie-comments">
		<div class="fb-comments" data-href="http://stream.dev/movie/taken_3" data-numposts="5" 	data-colorscheme="dark" 	
data-width="100%"></div>		
	</section>
@stop
