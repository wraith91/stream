@extends('layouts.app.master')

@section('contents')
	<section class="filtered-movies">
		<div class="inner-contents">
			<div class="movie-lists movie-grid">
				<div class="section-title">
					<h3><strong>Movies</strong> from {{ ucfirst($country) }}</h3>
				</div>
				@foreach ($movies as $movie)
					<div class="movie-item">
						<figure class="effect-chico">
							<img src="{{ asset('/main/images/upload/movie/posters/' . $movie->image) }}" alt="{{ $movie->title }}"/>
							<figcaption>
								<h2>{{ $movie->rating }} <span>rating</span></h2>
								<a href="/movie/{{ strtolower(preg_replace('/\s+/', '_', $movie->title)) }}" class="play-btn" data-method="get"></a>
								<a href="{{ $movie->trailer }}" class="swipebox-video trailer-btn" rel="youtube">Trailer</a>
								<a href="#" class="overlay">View more</a>
							</figcaption>		
						</figure>
						<div class="movie-title">
							<h4>{{ $movie->title }}</h4>
							<ul>
							@foreach ($movie->genres as $genre)
								<li><a href="{{ URL::to('/genre/' . strtolower($genre->name)) }}">{{ $genre->name }}</a></li>
							@endforeach
							</ul>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</section>
@stop
